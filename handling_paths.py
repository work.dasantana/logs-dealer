import os
from sys import platform

def path_handler(given_path):
    """
    Finds .log files within the given path

    :return: list of .log files
    """
    if not os.path.exists(given_path):  # Returns true if the path is a file, directory, or a valid symlink.
        raise ValueError('Path given not valid')

    else:
        if os.path.isfile(given_path) and given_path.endswith(
                '.log'):  # Returns true if the path is a regular file or a symlink to a file.
            log_path = [given_path]

        elif os.path.isdir(given_path):  # Returns true if the path is a directory or a symlink to a directory.

            files_in_path = os.listdir(given_path)  # Generates a list with all the files in the given path

            log_files = list(filter(lambda x: x.endswith('.log'), files_in_path))  # Filter .log files

            if platform.startswith('win'):
                separator = '\\'
            else:
                separator = '/'

            log_path = [given_path + separator + log_file for log_file in log_files]  # Create a list of paths to files

            if len(log_files) == 0:
                raise ValueError('No .log files where found')

        else:
            raise ValueError('No .log files where found ')

        return log_path


if __name__ == '__main__':
    path = os.path.abspath("test_directory/")
    print(path_handler(path))
