import unittest
import os

from handling_paths import path_handler


class TestPathHandler(unittest.TestCase):
    def test_path_to_file(self):
        # Check that the function correctly handles an absolute path to a specific file
        self.assertRaises(ValueError, path_handler, 'random string')
        self.assertEqual(path_handler(os.path.abspath("test_directory/example.log")),
                         [os.path.abspath("test_directory/example.log")])

    def test_path_to_dir(self):
        # Check that the function correctly handles an absolute path to a specific directory
        self.assertRaises(ValueError, path_handler, os.getcwd())
        self.assertEqual(list(path_handler(os.path.abspath("test_directory"))).sort(),
                         [os.path.abspath("test_directory/access.log"), os.path.abspath("test_directory/example.log")].sort())


if __name__ == '__main__':
    unittest.main()
