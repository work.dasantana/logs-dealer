FROM python:3.9

COPY . /app

WORKDIR /app

ENTRYPOINT ["python", "log_files_interpreter.py"]

