import os

import time
import json
import re
import argparse

from handling_paths import path_handler
from operations import LogOperations


def parse_arguments():
    """
    Permits the use of arguments when running script in the terminal

    :return: instance of parsed arguments from command-line
    """
    parser = argparse.ArgumentParser(description='A tutorial of argparse!')
    parser.add_argument("--mfip", action='store_true', help="Most frequent IP found within the logs")
    parser.add_argument("--lfip", action='store_true', help="Least frequent IP or list of IPs found within the logs")
    parser.add_argument("--eps", action='store_true', help="Events per second, calculated as the median within the Timestamp frame")
    parser.add_argument("--bytes", action='store_true', help="Total amount of bytes exchanged")

    return parser.parse_args()


def log_generator(file_path):
    """
    Creates a dictionary for each log line within a file given by it's path

    :return: dict with each element of the log as a key
    """

    file = open(file_path, encoding="utf8")

    file_logs = []
    for line in file:
        if not line.isspace():                                     #Avoid blank lines
            single_line = re.sub('\n', '', line, count=0, flags=0) #Eliminate de new lines character
            single_log = re.split(' +', single_line)               #Separate each element from log lines

            log_dict = {'Timestamp': single_log[0],
                        'Response Header size (bytes)': int(single_log[1]),
                        'Client IP address': single_log[2],
                        'HTTP response code': single_log[3],
                        'Response size (bytes)': int(single_log[4]),
                        'HTTP request method': single_log[5],
                        'URL': single_log[6],
                        'Username': single_log[7],
                        'Type of access/destination IP address': single_log[8],
                        'Response Type': single_log[9]}
            file_logs.append(log_dict)                             #Concatenate each log line data

    file.close()
    return file_logs


if __name__ == '__main__':

    path = './input'

    args = parse_arguments()             #Calls args for command-line execution

    log_files_paths = path_handler(path) #Get path to file or files to be evaluated

    whole_logs = [log_generator(file) for file in log_files_paths] #Create a dict for each line of .log files

    out_json = [n for x in whole_logs for n in x] #Concatenate dict from each line in a list to be stored as a JSON file

    path_to_be_stored = os.path.abspath('json_outputs')
    date_string = time.strftime("%Y-%m-%d-%H-%M") #Name for the file to be written
    name = date_string + '.json'
    writting_path = os.path.join(path_to_be_stored, name)

    with open(writting_path, 'w', encoding='utf-8') as f: #Write JSON file
        json.dump(out_json, f, ensure_ascii=False, indent=0)

    print('Absolute path to JSON file:', os.path.abspath(writting_path), '\n')

    results = LogOperations(out_json, args) #Create an instance of the operations to be printed as output if asked

    if args.mfip or args.lfip:
        results.most_and_least_frequent()

    if args.bytes:
        results.exchanged_bytes()

    if args.eps:
        results.events_per_second()

