import argparse

def parse_arguments():
    parser = argparse.ArgumentParser(description='A tutorial of argparse!')

    parser.add_argument('path', type=str, help='Path to .log file or containing directory')
    parser.add_argument("--mfip", action='store_true', help="Most frequent IP found within the logs")
    parser.add_argument("--lfip", action='store_true', help="Least frequent IP or list of IPs found within the logs")
    parser.add_argument("--eps", action='store_true', help="Events per second, calculated as the median within the Timestamp frame")
    parser.add_argument("--bytes", action='store_true', help="Total amount of bytes exchanged")

    return parser

