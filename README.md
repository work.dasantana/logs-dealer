<h1 align="center">LOG files interpreter</h1>
<p align="center">

>Welcome to my interview project.<br />
> This is an API that generates a `.json` file from `.log` files and gets some data out of it if asked

## What it does

`log-files-interpreter` is able to read a `.log` file or `.log` containing directory and group its data in a `.json` file within a previously designated location with the date of the consult as file name an then prints the absolute path to such file, and the results of the consult options (if asked).


Example of `.log` data to take as input:
```
1157689312.049   5000 10.100.10.100 CP_MISS/200 40000 CONNECT login.yahoo.com anon_user DIRECT/20.200.20.200 text/css
1157689320.327   6070 15.105.15.105 TCP_MISS/200 50000 GET http://www.google.com/ famous_user DIRECT/90.800.27.520 image/gif
```

 `.json` file to be given for that input:

```json
[
{
"Timestamp": "1157689312.049",
"Response Header size (bytes)": "5000",
"Client IP address": "10.100.10.100",
"HTTP response code": "TCP_MISS/200",
"Response size (bytes)": "40000",
"HTTP request method": "CONNECT",
"URL": "login.yahoo.com",
"Username": "anon_user",
"Type of access/destination IP address": "DIRECT/20.200.20.200",
"Response Type": "text/css"
},
{
"Timestamp": "1257829359",
"Response Header size (bytes)": "6070",
"Client IP address": "15.105.15.105",
"HTTP response code": "TCP_MISS/200",
"Response size (bytes)": "50000",
"HTTP request method": "CONNECT",
"URL": "google.com",
"Username": "famous_user",
"Type of access/destination IP address": "DIRECT/90.800.27.520",
"Response Type": "image/gif"
}
]
```


## How it's built
As requested, this project is built to work as a container (Docker).


## Usage
Make sure you have [Docker](https://www.docker.com/) installed.

If so, first, you would have to `build` a Docker container (from the project root):
```sh
docker build -t <container_name> .
```

Then, in the case of a directory, you would have to `run` the container and create a volume `-v` for an immutable path `/app/input` by typing the path to the `.log` file or `.log` containing directory as input data:
```sh
docker run -v <path/given/by/you>:/app/input <container_name>
```
In the case of a single file, the file name would have to be added to the end of the volume path:
```sh
docker run -v <path/given/by/you/file_name>:/app/input/file_name <container_name>
```

You can also use flags (after the container is built, or the path is given) to return data from the log files:
```sh
--mfip    Most frequent IP
--lfip    Least frequent IP
--eps     Events per second
--bytes   Total amount of bytes exchanged

Docker container example: docker run -v <path/given/by/you>:/app/input <container_name> --mfip --bytes

```

If so, a `.json`shoulded have been saved at the "/consults" directory with the date as name. 

## Author

👤 **Daniel Santana**

- Gitlab: [@work.dasantana](https://https://gitlab.com/work.dasantana)


## License

The code in this repository is given from me under an open source license Copyright ©2022.

---

_Hope you enjoy this project_
