from datetime import datetime


class LogOperations:
    def __init__(self, log_list, args):
        self.log_list = log_list
        self.mfip = args.mfip
        self.lfip = args.lfip
        self.eps = args.eps
        self.bytes = args.bytes

    def most_and_least_frequent(self):
        """
        Finds the least and most repeated values in a list of dictionaries

        :return: list for least and most frequent values, in case of both asked, returns least and most frequent
        values (in that order)
        """
        try:
            logs_ips = [i['Client IP address'] for i in
                        self.log_list]  # Create a list with all the IP's from the log lines
        except Exception as e:
            raise ValueError('Not a valid input. It has to be a list of dicts with "Client IP address" one of its keys')

        if not isinstance(logs_ips, list) and not isinstance(logs_ips, tuple):  # Check list format
            raise ValueError('Value given must be a list or a tuple')

        elements = set(logs_ips)  # Create a set for non-repeated IP list
        out_dict = {}
        for i in elements:
            out_dict[i] = logs_ips.count(
                i)  # Create a dict with each IP as key an the number of times is repeated as values

        if self.mfip and self.lfip:
            least_frequent = [k for k, v in out_dict.items() if v == min(list(out_dict.values()))]
            most_frequent = [k for k, v in out_dict.items() if v == max(list(out_dict.values()))]
            print('The least frequent IP is/are:', least_frequent, '\n')
            print('The most frequent IP is/are:', most_frequent, '\n')
            return least_frequent, most_frequent

        elif self.lfip:
            least_frequent = [k for k, v in out_dict.items() if v == min(list(out_dict.values()))]
            print('The least frequent IP is:', least_frequent, '\n')
            return least_frequent

        elif self.mfip:
            most_frequent = [k for k, v in out_dict.items() if v == max(list(out_dict.values()))]
            print('The least frequent IP is:', most_frequent, '\n')
            return most_frequent

    def exchanged_bytes(self):
        """
        Finds the least and most repeated values in a list of dictionaries

        :return: list for least and most frequent values, in case of both asked, returns least and most frequent
        values (in that order)
        """
        exchanged_bytes = [j['Response Header size (bytes)'] + j['Response size (bytes)'] for j in self.log_list]
        exchanged_bytes = sum(exchanged_bytes)
        print('Exchanged bytes:', exchanged_bytes, '\n')
        return exchanged_bytes

    def events_per_second(self):
        """
        Finds the least and most repeated values in a list of dictionaries

        :return: list for least and most frequent values, in case of both asked, returns least and most frequent
        values (in that order)
        """
        timestamps = [k['Timestamp'] for k in self.log_list]
        min_time = min(timestamps)
        max_time = max(timestamps)
        diff_time = datetime.fromtimestamp(float(max_time)) - datetime.fromtimestamp(float(min_time))
        diff_time = diff_time.total_seconds()
        events_per_second = len(self.log_list) / diff_time
        print('Events per second:', events_per_second, '\n')
        return events_per_second
