import unittest

from operations import LogOperations
from argument_parser import parse_arguments


class TestLogOperations(unittest.TestCase):

    def setUp(self):
        self.json_sample = [
            {
                "Timestamp": "1157689312.049",
                "Response Header size (bytes)": 5000,
                "Client IP address": "10.100.10.100",
                "HTTP response code": "TCP_MISS/200",
                "Response size (bytes)": 40000,
                "HTTP request method": "CONNECT",
                "URL": "login.yahoo.com",
                "Username": "anon_user",
                "Type of access/destination IP address": "DIRECT/20.200.20.200",
                "Response Type": "text/css"
            },
            {
                "Timestamp": "1257829359",
                "Response Header size (bytes)": 6070,
                "Client IP address": "15.105.15.105",
                "HTTP response code": "TCP_MISS/200",
                "Response size (bytes)": 50000,
                "HTTP request method": "CONNECT",
                "URL": "google.com",
                "Username": "famous_user",
                "Type of access/destination IP address": "DIRECT/90.800.27.520",
                "Response Type": "image/gif"
            },
            {
                "Timestamp": "1157679312",
                "Response Header size (bytes)": 8000,
                "Client IP address": "10.100.10.100",
                "HTTP response code": "TCP_MISS/200",
                "Response size (bytes)": 90000,
                "HTTP request method": "CONNECT",
                "URL": "login.yahoo.com",
                "Username": "anon_user",
                "Type of access/destination IP address": "DIRECT/20.200.20.200",
                "Response Type": "text/css"
            },
        ]
        self.parser = parse_arguments()

    def test_most_frequent(self):
        # Check that the function correctly finds most frquent element when asked
        args = self.parser.parse_args(['/random/path', '--mfip'])
        sample = LogOperations(self.json_sample, args)

        self.assertEqual(sample.most_and_least_frequent(), ['10.100.10.100'])

    def test_least_frequent(self):
        # Check that the function correctly finds least frquent element when asked
        args = self.parser.parse_args(['/random/path', '--lfip'])
        sample = LogOperations(self.json_sample, args)

        self.assertEqual(sample.most_and_least_frequent(), ["15.105.15.105"])

    def test_both_most_and_least_frequent(self):
        # Check that the function correctly finds both least and most frequent elements when asked and returns them in that specific order
        args = self.parser.parse_args(['/random/path', '--mfip', '--lfip'])
        sample = LogOperations(self.json_sample, args)

        self.assertEqual(sample.most_and_least_frequent(), (["15.105.15.105"], ['10.100.10.100']))

    def test_values(self):
        # Make sure value errors are raised when not a list or a tuple of dicts with the specific key:
        args = self.parser.parse_args(['/random/path', '--mfip', '--lfip'])
        sample = LogOperations(self.json_sample, args)

        self.assertEqual(sample.most_and_least_frequent(), (["15.105.15.105"], ['10.100.10.100']))
        with self.assertRaises(ValueError):
            LogOperations('random string', args).most_and_least_frequent()
            LogOperations({'random': 'dict'}, args).most_and_least_frequent()
            LogOperations(True, args).most_and_least_frequent()
            LogOperations(2 + 3j, args).most_and_least_frequent()
            LogOperations(7, args).most_and_least_frequent()
            LogOperations(6.7, args).most_and_least_frequent()

    def test_events_per_second(self):
        # Check that the class method correctly calculates the number of events per second
        args = self.parser.parse_args(['/random/path', '--eps'])
        sample = LogOperations(self.json_sample, args)

        self.assertAlmostEquals(sample.events_per_second(), 2.995613014608496e-08)

    def test_exchanged_bytes(self):
        # Check that the class method correctly adds the exchanged bytes for the given logs
        args = self.parser.parse_args(['/random/path', '--eps'])
        sample = LogOperations(self.json_sample, args)

        self.assertEqual(sample.exchanged_bytes(), 199070)


if __name__ == '__main__':
    unittest.main()
